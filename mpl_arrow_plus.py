#!/usr/bin/env python
"""MPL Arrow Plus: better Arrows for Matplotlib

Compared to the regular matplotlib Arrow class, using the arrow_plus function
draws an annotation, with a perspective preserving arrowhead. This means that no
matter the direction the arrow is pointing, the arrowhead shape and size is
consistent.

Information:
============
    Author: James Fallon
    Email: online@jamesfallon.eu
    Date: 2023-09-11, Last Updated 2023-09-11
"""

__author__ = "James Fallon"
__credits__ = ["James Fallon"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "James Fallon"
__email__ = "online@jamesfallon.eu"
__status__ = "Development"

import matplotlib as mpl
from numpy import sin, cos
from typing import Optional

def arrow_plus(
    px: float,
    py: float,
    qx: Optional[float]=None,
    qy: Optional[float]=None,
    dx: Optional[float]=None,
    dy: Optional[float]=None,
    theta: Optional[float]=None,
    r: Optional[float]=None,
    width: float=0.001,
    headwidth: float=1,
    headlength: float=1,
    shrink: float=1,
    color: str="black",
    connectionstyle: str="arc3",
    axes: Optional[mpl.axes.Axes]=None,
    **fancy_arrow_patch_kwargs) -> mpl.text.Annotation:
    """Arrow Plus function for matplotlib, using the annotation class.

    Parameters:
    ===========
    px, py : float
        Input arrow base position co-ordinates
    qx, qy : Optional[float]
        Input arrow head position co-ordinates
    dx, dy : Optional[float]
        Input arrow offsets from base to reach arrow head location
    theta, r : Optional[float]
        Input the angle and radius from base to reach arrow head location
    width, headwidth, headlength, shrink, **fancy_arrow_patch_kwargs: float
        Properties used to draw a mpl.patches.FancyArrowPatch
    color : str
        Arrow color
    connectionstyle : str
        Arrow connection style (matplotlib.patches.FancyArrowPatch)
    axes : Optional[matplotlib.axes.Axes]
        Axes to draw arrow

    Returns:
    ========
    ann : matplotlib.text.Annotation
        Arrow as a matplotlib.text.Annotation type object

    Usage:
    ======
    >>> stats = calculate_statistics(data, ["mean", "std"])
    """
    # get arrow head location, in terms of offsets dx, dy
    if dx is not None and dy is not None:
        pass
    elif qx is not None and qy is not None:
        dx = (qx - px)
        dy = (qy - py)
    elif theta is not None and r is not None:
        dx = r * cos(theta)
        dy = r * sin(theta)
    else:
        raise ValueError("Must pass arguments for arrow head location!")

    # define arrow properties
    arrow = dict(width=width, headwidth=headwidth, headlength=headlength,
                 shrink=shrink, color=color, connectionstyle=connectionstyle,
                 **fancy_arrow_patch_kwargs)

    # ensure axes is provided
    if axes is None:
        axes = mpl.plt.gca()

    # make the annotation
    ann = axes.annotate("", xytext=(px, py), xy=(px + dx, py + dy),
                        xycoords='data', textcoords='data', arrowprops=arrow)

    # return arrow as a matplotlib.text.Annotation object
    return ann 
